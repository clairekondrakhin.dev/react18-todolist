/* Import de prop-types */
import PropTypes from "prop-types";

/* Import du composant TodoItem */
import TodoItem from "./TodoItem";



function TodoList(props) {
  const { todos } = props;

  /* Je crée une variable listTodo qui permet de parcourir le taleau todos avec map pour créer une liste d'éléments TodoItem, où chaque élément a des props spécifiques (key, title, completed).*/
  /* key aide à identifier de manière unique chaque élément de la liste lors du rendu. La key ne sera en revanche pas affichée*/
  const listTodo = todos.map((todo) => (
    <TodoItem key={todo.id} title={todo.title} completed={todo.completed}>
      {" "}
    </TodoItem>
  ));

  /* j'affiche le titre de la liste de todos avec le nombre d'items et la liste (titre + 'to do' ou 'is done') */
  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>{listTodo}</ul>
    </div>
  );
}

/* Validation des types des props de type tableau avec PropTypes */
TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
