import PropTypes from "prop-types";

function TodoItem(props) {
  const { title, completed,color } = props;
  return (
    <li>
      <span>{title}</span> ({completed ? "is done" : "to do"})
    </li>
  );
}

TodoItem.propTypes = {
  title: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
};

export default TodoItem;
